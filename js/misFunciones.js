async function leerJSON(url) {

    try {
        let response = await fetch(url);
        let user = await response.json();
        return user;
    } catch (err) {

        alert(err);
    }
}
const parametro = new URLSearchParams(window.location.search);
const butto = document.querySelector('.btn-primary');
var def = 0;

butto.addEventListener('click', () => {
    let not = document.getElementsByName('datos');
    location.href = 'html/mostrarNotas.html?codigo=' + not[0].value + '&eliminar=' + not[1].value + '';
})

function lecturaDatos() {
    let url = "https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/json/estudiantes.json";

    leerJSON(url).then(datos => {
        drawTableNotas(datos.estudiantes, datos.descripcion);
        drawTableAlumno(datos.estudiantes, datos.nombreMateria);
        drawChartPie();

    });
}

function indexof(estudiantes, select, cod) {
    let codEstudiante;
    if (select == false) {
        codEstudiante = cod;
    } else {
        codEstudiante = parametro.get('codigo');
    }

    let index = -1;
    for (let i = 0; i < estudiantes.length; i++) {
        if (estudiantes[i].codigo == codEstudiante) {
            index = i;
        }
    }
    
    if (index == -1 && select == true) {
        alert("El código que digito es invalido, por favor vuelva a intentarlo");
        location.href = '../index.html';
    }else if(index == -1 && select == false){
        alert("El código que digito es invalido, por favor vuelva a intentarlo");
        location.href = 'index.html';
    }
    return index;
}

function drawTableAlumno(estudiantes, materia) {
    var data = new google.visualization.DataTable();
    data.addColumn('string');

    data.addRows([
        [`Nombre : ${estudiantes[indexof(estudiantes,true,0)].nombre}`],
        [`Código : ${estudiantes[indexof(estudiantes,true,0)].codigo}`],
        [`Materia : ${materia}`],
    ]);

    var table = new google.visualization.Table(document.getElementById('table_info'));

    table.draw(data, { showRowNumber: false, width: '40%', height: '100%' });
}

var auxnotas = [];
var descr = [];

function drawTableNotas(estudiantes, descripcion) {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'descripción');
    data.addColumn('number', 'valor');
    data.addColumn('string', 'observación');

    data.addRows(llenarTabla(estudiantes, descripcion, 1,-1,-1));

    var table = new google.visualization.Table(document.getElementById('table_notas'));

    table.draw(data, { showRowNumber: false, width: '50%', height: '100%' });
}

var cnt = 0;
function llenarTabla(estudiantes, descripcion, valorEliminar, valueEliminar, cod) {
    let arregloDatos = [];
    let notas = [descripcion.length];
    auxnotas = [descripcion.length];
    descr = [descripcion.length];
    let tmpNotas = [descripcion.length];
     cnt = descripcion.length;
    var cntEliminar = 0,
        indice = 0;
    if (valorEliminar == false) {
        cntEliminar = valueEliminar;
    } else {
        cntEliminar = parametro.get('eliminar');
    }

    if (cntEliminar < cnt && cntEliminar >= 1) {
        for (let j = 0; j < estudiantes[indexof(estudiantes, valorEliminar, cod)].notas.length; j++) {
            let nota = estudiantes[indexof(estudiantes, valorEliminar, cod)].notas[j].valor;
            notas[j] = nota;
            auxnotas[j] = nota;
            tmpNotas[j] = nota;
            def += nota;
            if (nota < 3) {
                descr[j] = "Nota reprobada";
            } else {
                descr[j] = "Nota aprobada";
            }
        }
        auxnotas.sort();
        let index = 0;
        while (cntEliminar > 0) {
            indice = tmpNotas.indexOf(auxnotas[index]);
            descr[indice] = "Nota eliminada";
            def -= tmpNotas[indice];
            delete tmpNotas[indice];
            index++;
            cnt--;
            cntEliminar--;
        }
        for (i = 0; i <= descripcion.length; i++) {
            let estudian = [];
            if (i != descripcion.length) {
                estudian.push(descripcion[i].descripcion);
                estudian.push(notas[i]);
                estudian.push(descr[i]);
                arregloDatos.push(estudian);
            } else {
                estudian.push('NOTA DEFINITIVA');
                def /= cnt;
                estudian.push(def);
                estudian.push('NOTA DEFINITIVA');
                arregloDatos.push(estudian);
            }
        }
    } else {
        alert("la cantidad de notas a eliminar es invalido, por favor vuelva a intentarlo");
        if(valorEliminar == false){
            location.href = 'index.html';
        }else{
            location.href = '../index.html';
        }
    }
    return arregloDatos;
}

function llenarTablaNotas() {
    let aprobados = 0;
    for (let i = 0; i < descr.length; i++) {
        if (descr[i] == "Nota aprobada") aprobados++;
    }
    return aprobados;

}

function drawChartPie() {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'estado');
    data.addColumn('number', 'nota');
    data.addRows(2);
    data.setCell(0, 0, "Aprobados");
    data.setCell(0, 1, llenarTablaNotas());
    data.setCell(1, 0, "Reprobados");
    data.setCell(1, 1, cnt - llenarTablaNotas());
    var options = {
        title: 'Información de estudiantes',
        is3D: true
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart'));

    chart.draw(data, options);
}

function verDefinitiva() {
    let modal = document.getElementById('myModal');
    let datos = document.getElementsByName("datos");
    let cnt = datos[1].value;
    let cod = datos[0].value;
    let url = "https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/json/estudiantes.json";
    console.log(cod);
    console.log(cnt);
    leerJSON(url).then(datos => {
        llenarTabla(datos.estudiantes, datos.descripcion, false, cnt, cod);
    });
    modal.addEventListener('mouseout', function(event) {
        let titul = document.getElementById('tituloModal');
        titul.innerHTML = "Definitiva";
        document.getElementById('contenidoModal').innerHTML = "Su definitiva es de: " + def.toFixed(2);
    })
    def = 0;
}